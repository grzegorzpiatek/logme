"use strict";
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        "chromeExtension/popup": "./chromeExtension/index.js",
        "snippet/snippet": "./snippet/snippet.js"
    },
    output: {
        path: path.resolve('dist'),
        publicPath: "/",
        filename: "[name].js"
    },
    plugins: [
        new CleanWebpackPlugin(['dist'], {})
    ],
    module: {
        rules: [{
                test: /\manifest.json$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            }, {
                test: /\.html|css$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            }, {
                test: /\.(png|svg|ico|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            },
            {
                test: /\config.json$/,
                loader: 'json-loader'
            }
        ]
    }
};