import * as Config from "../config.json";

document.addEventListener('DOMContentLoaded', function () {

    var updateLocalStorage = function (token) {
        chrome.tabs.create({
            active: false,
            url: Config.localhost
        }, function (tab) {
            chrome.tabs.executeScript(tab.id, {
                code: 'localStorage.setItem("' + Config.storageVarName + '", ' + JSON.stringify(token) + ');'
            }, function () {
                chrome.tabs.remove(tab.id);
                showSuccessMsg();
            });
        });
    };

    var updateToken = function () {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                updateLocalStorage(xmlHttp.response);
            } else if (xmlHttp.readyState == 4) {
                showErrorMsg();
            }
        }
        xmlHttp.open("GET", Config.authApiUrl, false);
        xmlHttp.send(null);
    };

    var showSuccessMsg = function () {
        document.getElementById('loading').style.display = "none";
        document.getElementById('success').style.display = "block";
    };

    var showErrorMsg = function () {
        document.getElementById('loading').style.display = "none";
        document.getElementById('error').style.display = "block";
    }

    updateToken();
}, false);