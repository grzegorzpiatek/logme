# Log Me

Tool to refresh authorization token

## About

Tool to refresh authorization token for localhost projects.

## How to use

Before build project is necessary to fill `config.json` file.

### Chrome extension

You can use this tool it as a chrome extension. To do it just make some simple steps:

1. Open `chrome://extensions/` tab in chrome browser.

2. Turn on "Developer mode".

3. Choose `Load unpacked extension..` option.

4. Point exstension folder `REPO_DIR/dist/chromeExstension`.

After all steps new chrome extension should be visible. To refresh token just click on extension icon.

### JS snippet

Copy all from `REPO_DIR/dist/snippet/snippet.js` and past into browser console.

### Bookmark button

To create Bookmark button make some steps:

1. Copy all from `REPO_DIR/dist/snippet/snippet.js`.

2. Add new bookmark in chrome.

3. As a `URL` write `javascript:` and past content coped in step 1.


To refresh token just click on bookmark button.