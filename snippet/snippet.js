import * as Config from "../config.json";

var xmlHttp = new XMLHttpRequest();
xmlHttp.onreadystatechange = function () {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
        localStorage.setItem(Config.storageVarName, xmlHttp.response);
    }
}
xmlHttp.open("GET", Config.authApiUrl, false);
xmlHttp.send(null);